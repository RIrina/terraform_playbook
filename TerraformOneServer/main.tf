data "vkcs_compute_flavor" "compute" {
  name = var.compute_flavor
}

data "vkcs_images_image" "compute" {
  name = var.image_flavor
}

resource "vkcs_compute_instance" "reactjs_server" {
  name = "reactjs_server"
  flavor_id = data.vkcs_compute_flavor.compute.id
  key_pair= var.key_pair_name
  security_groups = ["default", "ssh", "ssh+www"]
  availability_zone =  var.availability_zone_name


  block_device {
  uuid = data.vkcs_images_image.compute.id
  source_type = "image"
  destination_type = "volume"
  volume_type = "ceph-hdd"
  volume_size = 30
  boot_index = 0
  delete_on_termination = true
  }


  network {
  uuid = vkcs_networking_network.network.id
  fixed_ip_v4 = "10.0.0.25"
  }

}

#####
resource "vkcs_networking_floatingip" "fip" {
  pool = data.vkcs_networking_network.extnet.name
}

resource "vkcs_compute_floatingip_associate" "fip" {
  floating_ip = vkcs_networking_floatingip.fip.address
  instance_id = vkcs_compute_instance.reactjs_server.id
}

output "instance_fip" {
  value = vkcs_networking_floatingip.fip.address
}


