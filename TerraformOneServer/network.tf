
data "vkcs_networking_network" "extnet" {
   name = "ext-net"
}

resource "vkcs_networking_network" "network" {
   name = "net"
}

resource "vkcs_networking_subnet" "subnetwork" {
   name = "subnet_1619"
   network_id = vkcs_networking_network.network.id 
   cidr = "10.0.0.0/24"
}

resource "vkcs_networking_router" "router1" {
   name = "router1"
   admin_state_up = true
   external_network_id = data.vkcs_networking_network.extnet.id
}

resource "vkcs_networking_router_interface" "router1" {
   router_id = vkcs_networking_router.router1.id
   subnet_id = vkcs_networking_subnet.subnetwork.id
}

resource "vkcs_networking_secgroup" "secgroup" {
 
   name = "security_group"
   description = "terraform security group"

}

resource "vkcs_networking_port" "port" {  
   name = "port_1"
   admin_state_up = "true"
   network_id = vkcs_networking_network.network.id

}



